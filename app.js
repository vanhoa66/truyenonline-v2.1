const express = require("express");
const mongoose = require("mongoose");
const path = require("path");
const exphbs = require("express-handlebars");

const port = 3000;
const app = express();

const dbHost = process.env.DB_HOST || "localhost";
const dbPort = process.env.DB_PORT || 27017;
const dbName = process.env.DB_NAME || "truyenfull";
const dbUser = process.env.DB_USER;
const dbUserPassword = process.env.DB_PASSWORD;
const mongoUrl = `mongodb://${dbUser}:${dbUserPassword}@${dbHost}:${dbPort}/${dbName}`;

const connectWithRetry = function () {
  // when using with docker, at the time we up containers. Mongodb take few seconds to starting, during that time NodeJS server will try to connect MongoDB until success.
  return mongoose.connect(
    mongoUrl,
    { useNewUrlParser: true, useFindAndModify: false },
    (err) => {
      if (err) {
        console.error(
          "Failed to connect to mongo on startup - retrying in 5 sec",
          err
        );
        setTimeout(connectWithRetry, 5000);
      }
    }
  );
};
connectWithRetry();

// const MONGO_URL = " mongodb://localhost:27017/truyenfull";
// mongoose.set("useCreateIndex", true);
// mongoose
//   .connect(MONGO_URL, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
//   })
//   .catch((err) => {
//     throw err;
//   });
// Handlebars Helpers
const { chapList } = require("./helpers/hbs");
// Handlebars
app.engine(
  ".hbs",
  exphbs({
    helpers: {
      chapList,
    },
    defaultLayout: "main",
    extname: ".hbs",
  })
);
app.set("view engine", ".hbs");
// Static folder
app.use(express.static(path.join(__dirname, "public")));
// app.set("view engine", "pug");
// app.set("views", path.join(__dirname, "views"));
// app.use(express.static("public"));
// app.use("/", (req, res) => {
//   res.render("home");
// });
// Router
app.use(require("./routes/home.route"));
app.use(require("./routes/story.route"));
app.use(require("./routes/chap.route"));
app.use((req, res) => {
  res.status(404).redirect("/");
});

//app.listen(port, function () {
 // console.log("Server listening on port " + port);
//});
 module.exports = app;
