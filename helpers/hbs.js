const moment = require("moment");

module.exports = {
  formatDate: function (date, format) {
    return moment(date).utc().format(format);
  },
  truncate: function (str, len) {
    if (str.length > len && str.length > 0) {
      let new_str = str + " ";
      new_str = str.substr(0, len);
      new_str = str.substr(0, new_str.lastIndexOf(" "));
      new_str = new_str.length > 0 ? new_str : str.substr(0, len);
      return new_str + "...";
    }
    return str;
  },
  stripTags: function (input) {
    return input.replace(/<(?:.|\n)*?>/gm, "");
  },
  editIcon: function (storyUser, loggedUser, storyId, floating = true) {
    if (storyUser._id.toString() == loggedUser._id.toString()) {
      if (floating) {
        return `<a href="/stories/edit/${storyId}" class="btn-floating halfway-fab blue"><i class="fas fa-edit fa-small"></i></a>`;
      } else {
        return `<a href="/stories/edit/${storyId}"><i class="fas fa-edit"></i></a>`;
      }
    } else {
      return "";
    }
  },
  select: function (selected, options) {
    return options
      .fn(this)
      .replace(
        new RegExp(' value="' + selected + '"'),
        '$& selected="selected"'
      )
      .replace(
        new RegExp(">" + selected + "</option>"),
        ' selected="selected"$&'
      );
  },
  // chapList: function (story, chap, name) {
  //   return `<a href="${story}/${chap}" class="chaps__item"><h4>${name}</h4></a>`;
  // },
  chapList: function (story, chaps) {
    let chapList = "";

    for (let i = 0; i < 18; i++) {
      // console.log(chaps[i]);
      // let slug = chaps[i].slug;
      // let name = chaps[i].name;
      // console.log(chaps[i].slug);
      chapList += `<a href="${story.slug}/${chaps[i].slug}" class="chaps__item"><h4>${chaps[i].name}</h4></a>`;
    }
    chapList += "<div class='loading'>loading...<div>";
    // let chapList = "";
    // var elem = $(".loading");
    // var docViewTop = $(window).scrollTop();
    // var docViewBottom = docViewTop + $(window).height();
    // var elemTop = $(elem).offset().top;
    // var elemBottom = elemTop + $(elem).height();
    // if (elemBottom <= docViewBottom && elemTop >= docViewTop) {
    // $(".loading").remove();
    // var url = "https://randomuser.me/api/?results=10";
    // $.get(url, function (data) {
    // if (data.results) {
    // for (var i = 0; i < data.results.length; i++) {
    // var datum = data.results[i];
    // var n = datum.name;
    // var context = {
    //   img: datum.picture.medium,
    //   phone: datum.phone,
    //   age: datum.dob.age,
    //   dob: datum.dob.date,
    //   name: n.first + " " + n.last,
    //   gender: datum.gender,
    //   title: "My New Post",
    // };
    // console.log(context);
    // chapList += `<h4>${n.first + " " + n.last}</h4></a>`;
    // chapList += `<a href="${story}/${chap}" class="chaps__item"><h4>${name}</h4></a>`;
    // var card = "<div class='card'>";
    // card += template(context);
    // card += "</div>";
    // $(".result").append(card);
    // counter++;
    // updateCounter();
    // }
    //alert("Found: "+data.results[0].gender);
    // alert("Found: "+data.results.length);
    //   }
    // });
    // $(".result").append("<div class='loading'>loading...<div>");
    // }
    return chapList;
    // return `<a href="${story}/${chap}" class="chaps__item"><h4>${name}</h4></a>`;
  },
};
